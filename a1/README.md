> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Tyler Tolrud

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and BitBucket
2. Java/JSP/Servelt Development Installation
3. Chapter Questions (1-4)

#### README.md file should include the following items:

* Screenshot of running Java Hello (#1 above)
* Screenshot of running http://localhost.9999 (#2, step #4b in tutorial)
* git commands w descriptions
* bitbucket repo links a) this assignment and b) the completed tutorial above (bitbucketlocationstations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create and empty git repository on reinitialize and existing one
2. git status - show the working tree status
3. git add - add file contents to the index
4. git commit - record changes to the repository
5. git push - update remote refs along with associated objects
6. git pull - fetch from and inegrate with another repository or a local branch
7. git config - get and set repository or global options

#### Assignment Screenshots:

*Screenshot of Tomcat running http://localhost9999:

![AMPPS Installation Screenshot](img/tomcat_downloaded.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/Hello_world.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://tct14b@bitbucket.org/tct14b/bitbucketstationlocations.git/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://tct14b@bitbucket.org/tct14b/myteamquotes.git/ "My Team Quotes Tutorial")
