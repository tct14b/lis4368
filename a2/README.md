> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Tyler Tolrud

### Assignment 2 Requirements:

*Three parts:*

1. Assessement links 
2. Screenshot of query results 
3. Ch 5 and 6 questions

#### README.md file should include the following items:

* [http://localhost:9999/hello](http://localhost:9999/hello) (displays directory, needs index.html)

* [http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html)
(Rename "HelloHome.html" to "index.html")
* [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello) (invokes HelloServlet)
Note: /sayhello maps to HelloServlet.class (changed web.xml file)
* [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
* [http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi) (invokes AnotherHelloServlet)



#### Assignment Screenshots:

*Query Results*:

![Results set](img/query.png)




