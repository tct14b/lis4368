# LIS4368 Advanced Web Applications Development

## Tyler Tolrud

### Assignment 5 Requirements:

*Sub-Heading:*

1. Prepared statements to help prevent SQL injection.
2. JSTL to prevent XSS. Also adds insert functionality to A4
3. Screenshot of failed validation
4. Screenshot of passed validation
5. Questions for ch 13-15


#### Link to A4 localhost:

[A5 localhost link](http://localhost:9999/lis4368/customerform.jsp?assign_num=a5)

#### README.md file should include the following items:

![dbupdate](img/dbupdate.png)
![pre](img/pre.png)
![post](img/post.png)

