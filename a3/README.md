> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Tyler Tolrud

### Assignment 3 Requirements:

*Sub-Heading:*

1. Screenshot of petstore ERD
2. link to a3.mwb
3. link to a3.sql
4. CH 7 & 8 questions

#### README.md file should include the following items:

* [a3 MWB file](files/a3_lis4368.mwb)
* [a3 sql file](files/a3_lis4368.sql)




#### Assignment Screenshots:

*ERD screenshot*:

![ERD](img/a3_lis4368.png)
