> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Tyler Tolrud

### Project 2 Requirements:

*Sub-Heading:*

1. Screenshot of main splash page
2. Screenshot of passed validation
3. Screen shots of CRUD
4. These requirements complete the JSP/Servlets web application using the MVC framework and providing
create, read, update and delete (CRUD) functionality
5. CH 16 & 17 questions

#### README.md file should include the following items:

* [P1 Home Screen](http://localhost:9999/lis4368/index.jsp)




#### Assignment Screenshots:

*Validation Screenshots*:

![Verify](img/verify.png)
![Thanks](img/thanks.png)
![show](img/show.png)
![delete](img/delete.png)
![deleted](img/deleted.png)
![added](img/added.png)
