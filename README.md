> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Advanced Web Applications Development

## Tyler Tolrud

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Distributed Version Control with Git and BitBucket
    - Java/JSP/Servelt Development Installation
    - Chapter Questions (1-4)

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Assessment links
	- Query result screenshot
	- Ch 5 and 6 questions 

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Screenshot of petstore ERD
	- link to a3.mwb
	-link to a3.sql
	- Ch 7 and 8 questions 

4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Screenshot of main splash page
	- Screenshot of passed validation
	- link to project 1 home screen
	- Ch 9 and 10 questions 

5. [A4 README.md](a4/README.md "My A4 README.md file")
	- Add code to fix server-side validation
	- Compile the files 
	- Screenshot of failed validation
	- Screenshot of passed validation

6. [A5 README.md](a5/README.md "My A5 README.md file")
	- Prepared statements to help prevent SQL injection.
	- JSTL to prevent XSS. Also adds insert functionality to A4
	- Screenshot of failed validation
	- Screenshot of passed validation
	- Questions for ch 13-15

7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Screenshot of main splash page
	- Screenshot of passed validation
	- Screen shots of CRUD
	- These requirements complete the JSP/Servlets web application using the MVC framework and providing
	  create, read, update and delete (CRUD) functionality
	- CH 16 & 17 questions