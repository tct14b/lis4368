

# LIS4368 Advanced Web Applications Development

## Tyler Tolrud

### Assignment 4 Requirements:

*Sub-Heading:*

1. Add code to fix server-side validation
2. Compile the files 
3. Screenshot of failed validation
4. Screenshot of passed validation


#### Link to A4 localhost:

[A4 localhost link](http://localhost:9999/lis4368/customerform.jsp?assign_num=a4)

#### README.md file should include the following items:

![failed](img/failed.png)
![passed](img/passed.png)




